package com.example.bobo.mydemo2.Entity;

public class Shop {
    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "MyDatabase";

    public static final String TABLE_NAME = "SHOP";

    public static final String KEY_ID = "SHOP_ID";
    public static final String KEY_NAME = "SHOP_NAME";
    public static final String KEY_ADD = "SHOP_ADD";
    public static final String KEY_TARGET = "SHOP_TARGET";
    public static final String KEY_VISIT = "SHOP_VISIT";

    private int Id;
    private String Name;
    private String Add;
    private double Target;
    private String Visit;

    public Shop() {
    }

    public Shop(String NAME, String ADD, double TARGET, String VISIT) {
        this.Name = NAME;
        this.Add = ADD;
        this.Target = TARGET;
        this.Visit = VISIT;
    }

    public int getID() {
        return Id;
    }

    public String getNAME() {
        return Name;
    }

    public String getADD() {
        return Add;
    }

    public double getTARGET() {
        return Target;
    }

    public String getVISIT() {
        return Visit;
    }

    public void setID(int ID) {
        this.Id = ID;
    }

    public void setNAME(String NAME) {
        this.Name = NAME;
    }

    public void setADD(String ADD) {
        this.Add = ADD;
    }

    public void setTARGET(double TARGET) {
        this.Target = TARGET;
    }

    public void setVISIT(String VISIT) {
        this.Visit = VISIT;
    }
}
