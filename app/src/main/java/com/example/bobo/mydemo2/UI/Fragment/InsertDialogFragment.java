package com.example.bobo.mydemo2.UI.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bobo.mydemo2.Entity.Shop;
import com.example.bobo.mydemo2.Model.ShopModel;
import com.example.bobo.mydemo2.R;

public class InsertDialogFragment extends DialogFragment implements View.OnClickListener{
    EditText nameEditTxt;
    EditText addEditTxt;
    EditText targetEditTxt;
    Button okBtn;
    Button cancelBtn;
    ShopModel shopModel;
    Shop shop;
    Context context;

    public InsertDialogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.insert_fragment, container, false);
        this.context = getContext();
        shopModel = new ShopModel(context);
        findViews(v);
        return v;
    }

    private void findViews(View v) {
        nameEditTxt = v.findViewById( R.id.nameEditTxt );
        addEditTxt = v.findViewById( R.id.addEditTxt );
        targetEditTxt = v.findViewById( R.id.targetEditTxt );
        okBtn = v.findViewById( R.id.okBtn );
        cancelBtn = v.findViewById( R.id.cancelBtn );

        okBtn.setOnClickListener( this );
        cancelBtn.setOnClickListener( this );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.okBtn:
                if(check()){
                    shop = new Shop(nameEditTxt.getText().toString(),addEditTxt.getText().toString(),
                            Double.parseDouble(targetEditTxt.getText().toString()),"Còn hàng");
                    shopModel.addShop(shop);
                    dismiss();
                }
                break;
            case R.id.cancelBtn:
                this.dismiss();
                break;
        }
    }

    private boolean check(){
        if(TextUtils.isEmpty(nameEditTxt.getText())){
            Toast.makeText(context, "Chưa nhập tên", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(addEditTxt.getText())){
            Toast.makeText(context, "Chưa nhập địa chỉ", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(targetEditTxt.getText())){
            Toast.makeText(context, "Chưa nhập chỉ tiêu", Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            Toast.makeText(context, "Thêm thành công", Toast.LENGTH_SHORT).show();
            return true;
        }
    }
}
