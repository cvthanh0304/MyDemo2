package com.example.bobo.mydemo2.UI.Fragment;


import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bobo.mydemo2.Adapter.MyAdapter;
import com.example.bobo.mydemo2.Entity.Shop;
import com.example.bobo.mydemo2.Model.ShopModel;
import com.example.bobo.mydemo2.MyInterface;
import com.example.bobo.mydemo2.R;
import com.example.bobo.mydemo2.UI.ModelUI.MyItem;

import java.util.ArrayList;
import java.util.List;

public class ShopFragment extends Fragment implements View.OnClickListener, MyInterface {
    ImageView toolbarBackBtn;
    Button addOutletBtn;
    AutoCompleteTextView autoCompleteTextView;
    ImageButton nameSearchBtn;
    EditText dateEditTxt;
    ImageButton dateSearchBtn;
    RecyclerView recyclerView;
    TextView outletTextView;
    TextView visitTextView;

    ShopModel shopModel;
    MyAdapter myAdapter;
    List<MyItem> itemList;
    ArrayAdapter<String> adapter;
    List<String> nameList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.shop_fragment, container, false);
        shopModel = new ShopModel(getContext());
        fragmentInitial(v);
        loadData();
        return v;
    }

    private void fragmentInitial(View v) {
        toolbarBackBtn = v.findViewById(R.id.toolbarBackBtn);
        addOutletBtn = v.findViewById(R.id.addOutletBtn);
        autoCompleteTextView = v.findViewById(R.id.autoCompleteTextView);
        nameSearchBtn = v.findViewById(R.id.nameSearchBtn);
        dateEditTxt = v.findViewById(R.id.dateEditTxt);
        dateSearchBtn = v.findViewById(R.id.dateSearchBtn);
        recyclerView = v.findViewById(R.id.recyclerView);
        outletTextView = v.findViewById(R.id.outletTextView);
        visitTextView = v.findViewById(R.id.visitedTextView);
        itemList = new ArrayList<>();

        toolbarBackBtn.setOnClickListener(this);
        addOutletBtn.setOnClickListener(this);
        nameSearchBtn.setOnClickListener(this);
        dateSearchBtn.setOnClickListener(this);

        myAdapter = new MyAdapter(itemList, getContext(), this);
        recyclerView.setAdapter(myAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new BackgroundItemDecoration(R.color.itemBackgroundColor, R.color.whiteColor);
        recyclerView.addItemDecoration(itemDecoration);
    }

    void loadData() {
        nameList = new ArrayList<>();
        int notVisited = 0;
        List<Shop> shopList = shopModel.getAllShop();
        for (int i = 0; i < shopList.size(); i++) {
            itemList.add(new MyItem(shopList.get(i)));
            if (shopList.get(i).getVISIT().equals("Không ghé thăm")) {
                notVisited++;
                nameList.add(shopList.get(i).getNAME());
            }
        }

        adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, nameList);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setAdapter(adapter);

        String temp = String.valueOf(shopModel.getShopCount()) + " Outlets";
        outletTextView.setText(temp);
        temp = "Visited: " + String.valueOf(shopModel.getShopCount() - notVisited);
        visitTextView.setText(temp);
        myAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addOutletBtn:
                showInsertDialog();
                break;
            case R.id.toolbarBackBtn:
                getActivity().finish();
                break;
        }
    }

    private void showInsertDialog() {
        FragmentManager fm = getFragmentManager();
        InsertDialogFragment insertDialogFragment = new InsertDialogFragment();
        insertDialogFragment.show(fm, "a");
    }

    @Override
    public void updateVisit(int number, String state) {
        shopModel.updateVisit(number, state);
    }

    @Override
    public void openDetail(MyItem item) {
        FragmentManager fm = getFragmentManager();
        DetailFargment detailFargment = new DetailFargment(item);
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.fragment_container, detailFargment);
        transaction.addToBackStack(null);
        transaction.commit();
        //fm.beginTransaction().addToBackStack(null).add(R.id.fragment_container, detailFargment).commit();
    }

    public class BackgroundItemDecoration extends RecyclerView.ItemDecoration {

        private final int mOddBackground;
        private final int mEvenBackground;

        private BackgroundItemDecoration(@DrawableRes int oddBackground, @DrawableRes int evenBackground) {
            mOddBackground = oddBackground;
            mEvenBackground = evenBackground;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            int position = parent.getChildAdapterPosition(view);
            view.setBackgroundResource(position % 2 == 0 ? mEvenBackground : mOddBackground);
        }
    }
}
