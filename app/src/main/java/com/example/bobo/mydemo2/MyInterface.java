package com.example.bobo.mydemo2;

import com.example.bobo.mydemo2.UI.ModelUI.MyItem;

public interface MyInterface {
    void updateVisit(int number, String state);
    void openDetail(MyItem item);
}
