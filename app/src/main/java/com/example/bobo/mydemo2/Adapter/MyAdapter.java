package com.example.bobo.mydemo2.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bobo.mydemo2.MyInterface;
import com.example.bobo.mydemo2.R;
import com.example.bobo.mydemo2.UI.ModelUI.MyItem;

import java.text.DecimalFormat;
import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<MyItem> itemList;
    Context context;
    String money;
    String visit;
    ArrayAdapter adapter;
    MyInterface listener;
    DecimalFormat format = new DecimalFormat("#,###,###");

    public MyAdapter(List<MyItem> itemList, Context context, MyInterface listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
        adapter = ArrayAdapter.createFromResource(context, R.array.spinner_items, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.custom_item_row, parent, false);

        MyViewHolder holder = new MyViewHolder(itemView);
        itemView.setOnClickListener(holder);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyItem item = itemList.get(position);

        holder.numberTextView.setText(String.valueOf(item.getNumber()));
        holder.addressTextView.setText(item.getAddress());
        holder.shopNameTextView.setText(item.getShopName());
        money = format.format(item.getTarget()) + " VNĐ";
        holder.targetTextView.setText(money);
        holder.visitSpinner.setAdapter(adapter);
        holder.updatePosition(position);
        visit = item.getFailVisit();
        switch (visit) {
            case "Còn hàng":
                holder.visitSpinner.setSelection(0);
                break;
            case "Chủ đi vắng":
                holder.visitSpinner.setSelection(1);
                break;
            case "Đóng cửa":
                holder.visitSpinner.setSelection(2);
                break;
            case "Không ghé thăm":
                holder.visitSpinner.setSelection(3);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements Spinner.OnItemSelectedListener, View.OnClickListener {
        TextView numberTextView, addressTextView, shopNameTextView, targetTextView;
        Spinner visitSpinner;
        ImageButton imageButton;
        Context context2;
        private int position;

        public MyViewHolder(View view) {
            super(view);
            this.context2 = context;
            numberTextView = view.findViewById(R.id.numberTextView);
            shopNameTextView = view.findViewById(R.id.shopNameTextView);
            addressTextView = view.findViewById(R.id.addressTextView);
            targetTextView = view.findViewById(R.id.targetTextView);
            visitSpinner = view.findViewById(R.id.visitSpinner);
            imageButton = view.findViewById(R.id.imageButton);

            visitSpinner.setOnItemSelectedListener(this);
        }

        void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            MyItem item = itemList.get(position);
            int positionSpinner = visitSpinner.getSelectedItemPosition();
            listener.updateVisit(item.getNumber(), adapterView.getItemAtPosition(positionSpinner).toString());
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }

        @Override
        public void onClick(View view) {
            MyItem item = itemList.get(position);
            listener.openDetail(item);
        }
    }
}
