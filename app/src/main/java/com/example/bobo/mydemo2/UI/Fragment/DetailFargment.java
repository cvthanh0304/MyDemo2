package com.example.bobo.mydemo2.UI.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bobo.mydemo2.R;
import com.example.bobo.mydemo2.UI.ModelUI.MyItem;

public class DetailFargment extends Fragment implements View.OnClickListener {
    MyItem item;
    TextView shopNameTextView;
    ImageView toolbarBackBtn;

    public DetailFargment() {
    }

    @SuppressLint("ValidFragment")
    public DetailFargment(MyItem myItem) {
        this.item = myItem;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_fragment, container, false);

        fragmentInitial(v);

        return v;
    }

    private void fragmentInitial(View v) {
        shopNameTextView = v.findViewById(R.id.shopNameTextView);
        toolbarBackBtn = v.findViewById(R.id.toolbarBackBtn);

        toolbarBackBtn.setOnClickListener(this);
        shopNameTextView.setText(item.getShopName());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbarBackBtn:
                getFragmentManager().popBackStack();
                break;
        }
    }
}
