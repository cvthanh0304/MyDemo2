package com.example.bobo.mydemo2.UI.ModelUI;

import com.example.bobo.mydemo2.Entity.Shop;

public class MyItem {
    private int number;
    private double target;
    private String shopName;
    private String address;
    private String Visit;

    public MyItem(int number, double target, String shopName, String address, String failVisit) {
        this.number = number;
        this.target = target;
        this.shopName = shopName;
        this.address = address;
        this.Visit = failVisit;
    }

    public MyItem(Shop shop){
        this.number = shop.getID();
        this.target = shop.getTARGET();
        this.shopName = shop.getNAME();
        this.address = shop.getADD();
        this.Visit = shop.getVISIT();
    }

    public int getNumber() {
        return number;
    }

    public double getTarget() {
        return target;
    }

    public String getShopName() {
        return shopName;
    }

    public String getAddress() {
        return address;
    }

    public String getFailVisit() {
        return Visit;
    }
}
