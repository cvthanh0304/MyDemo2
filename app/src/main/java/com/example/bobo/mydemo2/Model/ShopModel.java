package com.example.bobo.mydemo2.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.bobo.mydemo2.Entity.Shop;

import java.util.ArrayList;
import java.util.List;

public class ShopModel extends SQLiteOpenHelper {
    private SQLiteDatabase db;

    public ShopModel(Context context) {
        super(context, Shop.DATABASE_NAME, null, Shop.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;

        String CREATE_TABLE_SHOP = "CREATE TABLE " + Shop.TABLE_NAME + "("
                + Shop.KEY_ID + " INTEGER PRIMARY KEY,"
                + Shop.KEY_NAME + " TEXT,"
                + Shop.KEY_ADD + " TEXT,"
                + Shop.KEY_TARGET + " DECIMAL(18,6),"
                + Shop.KEY_VISIT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE_SHOP);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String DROP_TABLE_SHOP = "DROP TABLE IF EXISTS" + Shop.TABLE_NAME;
        db.execSQL(DROP_TABLE_SHOP);
        onCreate(db);
    }

    public void addShop(Shop shop) {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Shop.KEY_NAME, shop.getNAME());
        values.put(Shop.KEY_ADD, shop.getADD());
        values.put(Shop.KEY_TARGET, shop.getTARGET());
        values.put(Shop.KEY_VISIT, shop.getVISIT());

        db.insert(Shop.TABLE_NAME, null, values);

        db.close();
    }

    public int getShopCount() {
        db = this.getReadableDatabase();

        String countQuery = "SELECT * FROM " + Shop.TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery, null);

        return cursor.getCount();
    }

    public List<Shop> getAllShop() {
        List<Shop> result = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + Shop.TABLE_NAME;

        db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext()) {
            do {
                Shop shop = new Shop();
                shop.setID(cursor.getInt(0));
                shop.setNAME(cursor.getString(1));
                shop.setADD(cursor.getString(2));
                shop.setTARGET(cursor.getDouble(3));
                shop.setVISIT(cursor.getString(4));

                result.add(shop);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return result;
    }

    public void updateVisit(int number, String state) {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Shop.KEY_VISIT, state);

        db.update(Shop.TABLE_NAME, values, Shop.KEY_ID + "=" + number, null);

        db.close();
    }

    public void updateShop(){

    }
}
